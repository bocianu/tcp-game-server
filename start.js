const net = require('net');
const config = require('./config.json');
const r = require('rethinkdb');
const game = require('./gameserver.js');

let lastId = 0;

const getNewId = () => {
    const id = lastId;
    lastId += 1;
    if (lastId == 0xffff) {
        lastId = 0;
    }
    return id;
}

const server = net.createServer();
r.connect(config.dba, function(err, db_conn) {
    if (err) throw err;
    console.log('Rethink Database connected.');
    const gameServer = game.createServer(r, db_conn, server);

    server.on('connection', conn => {
        console.log(`New connection from ${(conn.remoteAddress)}:${conn.remotePort}`);
        conn.id = getNewId();
        conn.uid = null;
        conn.setTimeout(config.TCP_timeout);
        conn.on('data', data => {
    
            if (gameServer.isAuthorized(conn, data)) {
                console.log(`Authorized user from: ${(conn.remoteAddress)}:${conn.remotePort}`);
                gameServer.processRequest(conn, data);
            } else {
                console.log(`Unauthorized user connection from: ${(conn.remoteAddress)}:${conn.remotePort}`);
                if (String.fromCharCode(data[0])=='A') {
                    console.log("authorization request");
                    gameServer.authorizeUser(conn, data);
                }
            }
   
        });

        conn.on('close', hadError => {
            if (hadError) console.log(`!!!Transmission Error!!!`);
            console.log(`Closed connection from ${(conn.remoteAddress)}:${conn.remotePort}`);
            gameServer.disconnectUser(conn);
        })

        conn.on('timeout', () => {
            console.log(`Connection timeouted from ${(conn.remoteAddress)}:${conn.remotePort}`);
            gameServer.disconnectUser(conn);
        })


    });
    
    server.on('error', err => {
        console.log(`!!!Error encountered!!!\n`)
        throw err;
    });
    
    server.listen(config.port);
    
    console.log(`Game server running at port ${config.port}.\n`);

});
