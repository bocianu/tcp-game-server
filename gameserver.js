const _ = require('lodash');
const stub = () => true;

userModel = {
    name:"",
    level:1,
    maxhp:10,
    hp:10,
    xp:0,
    location:0,
    position:0
};

locationModel = {
    idx : 0,
    name: "",
    colors: [0x1e, 0x1a, 0x16, 0x0, 0x0],
    data: [
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 4, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
    ],
    nav: [ 1, 2, 3, 4],
    ports: [
        {
            position: 8 + 10*4,
            target: {
                location:0,
                position:0
            }
        },
    ],
    spawners: [
        {
            delay: 100,
            limit: 2,
            mob: 0
        }
    ]
}

tileModel = {
    idx : 0,
    walkable: 1,
    data: [
        0b00000000,0b00000000, 
        0b00000000,0b00000000, 
        0b00000000,0b00000000, 
        0b00000000,0b00000000, 
        0b00000000,0b00000000, 
        0b00000000,0b00000000, 
        0b00000000,0b00000000, 
        0b00000000,0b00000000
    ],
    tile: [
        0,0,0,0,0,0,0,0, 
        0,0,0,0,0,0,0,0, 
        0,0,0,0,0,0,0,0, 
        0,0,0,0,0,0,0,0, 
        0,0,0,0,0,0,0,0, 
        0,0,0,0,0,0,0,0, 
        0,0,0,0,0,0,0,0, 
        0,0,0,0,0,0,0,0
    ]
}

const createServer = (rdb, db_conn, server) => {

    const reqStatus = stub;
    const reqGet = stub;
    const reqMove = stub;

    const loggedUsers = new Set;
    const activeLocations = new Set;

    const disconnectUser = (conn) => {
        conn.destroy();
        delete conn.uid;
        delete conn.id;
        loggedUsers.delete(conn.uid);
    };
    
    const sendFrame = (conn, cmd, len, data) => {
        const frame = new Uint8Array(3 + len);
        const u8data = new TextEncoder("utf-8").encode(data);
        frame[0] = cmd.charCodeAt(0);
        frame[1] = len & 0xff; // lower byte first
        frame[2] = (len >> 8) & 0xff; // upper byte
        frame.set(u8data, 3); // append remaining data block
        //console.log(frame);
        conn.write(frame);
    }
    
    const sendText = (conn, str) => {
        sendFrame(conn, 'T', str.length, str);
    }
    
    const sendUserData = (conn, uid) => {
        rdb.db('fujiland').table('users').get(uid).
        run(db_conn, function(err, response) {
            if (err) throw err;
            const res = JSON.stringify(response);
            console.log(res);
            sendFrame(conn, 'U', res.length, res);
        });
    }

    const sendError = (conn, msg) => {
        sendText(conn, `ERROR: ${msg}`);
    }
    
    const validCommands = [
        'A', // authorize
        'S', // get server status
        'U', // get user status
        'G', // get map data
        'M', // move user
        'Q'  // disconnect user
    ]
    
    const validResponses = [
        'T', // show text
        'M', // map data
        'G', // tile GFX data
        'S', // server status
        'U', // user status
        'E' // error 
    ]
    
    const isValidCommand = cmd => _.includes(validCommands, cmd);
    
    const parseRequest = data => {
        //console.log(data);
        const cmd = String.fromCharCode(data[0]);
        if (!isValidCommand(cmd)) {
            console.log(`unknown command`);
            return false;
        } 
        const datalen = data[1] + (data[2] * 0x100);
        const cmdData = _.slice(data,3);
        /*if (cmdData.length != datalen) {
            console.log(`invalid data size: ${cmdData.length} != ${datalen}`);
            return false;
        }*/
        return {
            cmd,
            datalen,
            data: cmdData
        }
    }
    

    const isAuthorized = (conn, request) => {
        return (conn.uid != null);
    };

    const authorizeUser = (conn, data) => {
        const request = parseRequest(data);
        if (request) {
            const uidstr = String.fromCharCode(...request.data);
            const [uname, uid]= _.split(uidstr,'#');
            //console.log('looking for',uname);
            rdb.db('fujiland').table('users').filter(rdb.row('name').eq(uname)).
            run(db_conn, function(err, cursor) {
                if (err) throw err;
                cursor.toArray(function(err, result) {
                    if (err) throw err;
                    //console.log(result);
                    if ((result.length > 0) && (result[0].id === uid)) {
                        console.log('user-found');
                        conn.uid = uid;
                        loggedUsers.add(conn.uid);
                    } else {
                        console.log('user-not-found');
                    }
                    if ((result.length == 0) && (uid == 'newuser') && (uname.length>0)) {
                        const newUser = _.cloneDeep(userModel);
                        newUser.name = uname;
                        rdb.db('fujiland').table('users').insert(newUser).
                        run(db_conn, function(err, result) {
                            if (err) throw err;
                            console.log('user-added');
                            console.log(result);
                            sendUserData(conn, result.generated_keys[0]);
                        });
                    }
                });
            });
        } else {
            console.log('bad authorization request');
        }
    };

    const executeRequest = (conn, request) => {
        sendText(conn, `ID:${conn.id}\n`);
        switch (request.cmd) {
            case 'A':
                    sendText(conn, 'Already authorized. Disconnect first.');
                break;
            case 'S':
                    reqStatus(conn, request);
                break;
            case 'G':
                    reqGet(conn, request);
                break;
            case 'M':
                    reqMove(conn, request);
                break;
            case 'Q':
                disconnectUser(conn);
            break;
            default:
                break;
        }
    }
    
    const processRequest = (conn, data) => {
        const request = parseRequest(data);
        if (request) {
            console.log(JSON.stringify(request));
            executeRequest(conn, request);
        } else {
            console.log('cannot parse request');
            sendError(conn, 'cannot parse request'); 
        }
    }
 
    return {
        isAuthorized,
        authorizeUser,
        disconnectUser,
        processRequest
    }

}

module.exports = {
    createServer
}